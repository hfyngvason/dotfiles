################################################################################
# Path & environment
################################################################################

#export GDK_RUNIT=1

# user bin path
export PATH="$PATH:$HOME/.local/bin:$HOME/bin"

# doom emacs
export PATH="$PATH:$HOME/.emacs.d/bin"

# Rust / Cargo
export PATH="$HOME/.cargo/bin:$PATH"
# command -v sccache >/dev/null 2>&1 && export RUSTC_WRAPPER=sccache

# golang
export PATH="$HOME/go/bin:$PATH"

# use macvim's vim in the terminal
if [ -f /usr/local/bin/mvim ]; then
  alias vim="mvim -v"
fi

# Initialize rbenv and nodenv
eval "$(rbenv init -)"
eval "$(nodenv init -)"

################################################################################
# Completions
################################################################################

# support both homebrew and linux default completion setup paths
COMPLETION_FILE_PATHS="
  /usr/local/etc/bash_completion
  /usr/share/bash-completion/bash_completion
  /usr/local/share/bash-completion/bash_completion
  /usr/share/git-core/contrib/completion/git-prompt.sh
  "

for completion_path in $COMPLETION_FILE_PATHS; do
  if [ -f $completion_path ]; then
    source $completion_path
  fi
done

################################################################################
# Appearance
################################################################################

export TERM=xterm-256color
export CLICOLOR=1

# tput setaf $color -- set foreground
# tput setab $color -- set background
# tput bold         -- make bold
# tput sgr0         -- reset formatting

rainbow() {
  color=0
  for row in {0..15}; do
    for col in {0..15}; do
      tput setab $color
      printf " %3d " $color
      color=$(( $color + 1 ))
    done
    echo
  done
  tput sgr0
}

__fmt_bold="\[$(tput bold)\]"
__fmt_fg_red="\[$(tput setaf 1)\]"
__fmt_fg_gold="\[$(tput setaf 3)\]"
__fmt_fg_dark="\[$(tput setaf 239)\]"
__fmt_fg_white="\[$(tput setaf 255)\]"
__fmt_reset="\[$(tput sgr0)\]"

tabcolor() {
  case "$(pwd)" in
    *gdk-ce*)
      # green
      r=139
      g=215
      b=137
      ;;
    *gdk-ee*)
      # orange
      r=238
      g=157
      b=56
      ;;
    *www-gitlab-com*)
      # purple
      r=180
      g=139
      b=207
      ;;
  esac
  if [[ -n "$r" ]]; then
    rc="\033]6;1;bg;red;brightness;$r\a"
    gc="\033]6;1;bg;green;brightness;$g\a"
    bc="\033]6;1;bg;blue;brightness;$b\a"
    echo -en "$rc$gc$bc"
  else
    echo -en "\033]6;1;bg;*;default\a"
  fi
}

export PROMPT_DIRTRIM=2
export PS1="${__fmt_bold}${__fmt_fg_gold}\u${__fmt_fg_dark}@${__fmt_fg_red}\h ${__fmt_fg_white}\w${__fmt_fg_dark}"'$(__git_ps1)$(tabcolor)'"\n\$ ${__fmt_reset}"
