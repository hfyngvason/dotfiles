set guioptions-=T " no toolbar
set guioptions-=m " no menubar
set guifont=Source\ Code\ Pro\ 11
set clipboard=unnamedplus " use system clipboard
let delimitMate_expand_cr=1
