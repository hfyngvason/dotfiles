export PATH="$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH"

# GDK stuff
export PATH="/usr/local/opt/postgresql@11/bin:$PATH"
export PATH="/usr/local/opt/node@14/bin:$PATH"
export PATH="/usr/local/opt/postgresql@11/bin:$PATH"
export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig:$PKG_CONFIG_PATH"

# gcloud
if [ -f '/Users/hfyngvason/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/hfyngvason/google-cloud-sdk/path.zsh.inc'; fi
if [ -f '/Users/hfyngvason/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/hfyngvason/google-cloud-sdk/completion.zsh.inc'; fi

# Rust / Cargo
export PATH="$HOME/.cargo/bin:$PATH"

# Golang
export PATH="$HOME/go/bin:$PATH"
export GO111MODULE=auto

# doom emacs
export PATH="$HOME/.emacs.d/bin:$PATH"

# Use neovim as default EDITOR when available
if [ -z "$EDITOR" ]; then
  if [ "-x $(command -v nvim)" ]; then
    export EDITOR='nvim'
  else
    export EDITOR='vim'
  fi
fi

# brew install zsh-autosuggestions
source /usr/local/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# brew install zsh-completions
if type brew &>/dev/null; then
  FPATH=$(brew --prefix)/share/zsh-completions:$FPATH

  autoload -Uz compinit
  compinit
fi
